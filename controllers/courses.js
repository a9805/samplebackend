//[SECTION] Dependencies and Modules
  const Course = require('../models/course');

//[SECTION] FUNCTIONALITY [CREATE]
   module.exports.createCourse = (info) => {
     let cName = info.name;
     let cDesc = info.description;
     let cCost = info.price;
     let newCourse = new Course({
      name: cName,
      description: cDesc,
      price: cCost
     }) 
     
     return newCourse.save().then((savedCourse, error) => {
      if (error) {
        return 'Failed to Save New Document';
      } else {
        return savedCourse; 
      }
     });
   };

//[SECTION] FUNCTIONALITY [RETRIEVE]
   //Retrieve all Courses
   module.exports.getAllCourse = () => {
        return Course.find({}).then(result => {
        return result;
     });
   };

   //Retrive a single course
  module.exports.getCourse = (id) => {
    return Course.findById(id).then(resultOfQuery => {
        return resultOfQuery;
    })
  };

  // Retrive all courses that are active
  module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result=>{
      return result;
    });
  }
   

//[SECTION] FUNCTIONALITY [UPDATE]
    module.exports.updateCourse = (id, details) => {
        
        let cName = details.name;
        let cDesc = details.description;
        let cPrice = details.price;

        let updatedCourse = {
          name: cName,
          description: cDesc,
          price: cPrice
        }

      return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
        
        if (err) {
          return `Failed to update. Error:`
        } else {
          return  `ID: ${id} has been successfully updated`;
        }

      });
    }
  
    module.exports.deactivateCourse = (id) => {
      
      let updates = {
        isActive: false
      }

      return Course.findByIdAndUpdate(id, updates).then((archived, err) => {
        if (archived) {
          return `The course ${id} has been deactivated`
        } else {
          return `Failed to acrchive course: ${id}`
        }
      })
    }

     module.exports.reactivateCourse = (id) => {
      
      let updates = {
        isActive: true
      }

      return Course.findByIdAndUpdate(id, updates).then((reactivatedCourse, err) => {
        if (reactivatedCourse) {
          return `The course ${id} has been re-activated`
        } else {
          return `Failed to reactivate course with course id: ${id}`
        }
      })
    }


//[SECTION] FUNCTIONALITY [DELETE]
 module.exports.deleteCourse = (id) =>{

    return Course.findByIdAndRemove(id).then((removedCourse, err)=>{
      if (err) {
        return 'No course deleted';
      } else {
        return `course has been deleted`;
      }
    });
  }
