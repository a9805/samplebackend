// [SECTION] Dependencies and modules
	const User = require('../models/user')
	const bcrypt = require("bcrypt");  
	const dotenv = require("dotenv"); 

// [SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

// [SECTION] Functionalities [CREATE]
	
	module.exports.registerUser = (data) => {
		
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let gendr = data.gender;
		let mobil = data.mobileNo;

		let newUser = new User({
			
				firstName: fName,
				lastName: lName,
				email: email,
				password: bcrypt.hashSync(passW,salt),
				gender: gendr,
				mobileNo: mobil
		});

		return newUser.save().then( (userCreated, rejected) => {
			if (userCreated) {
				return userCreated;
			} else {
				return 'Failed to Register new account'
			}	
		});
	};

// [SECTION] Functionalities [RETRIEVE]

	module.exports.getAllUsers = () => {
        return User.find( {} ).then(result => {
        return result;
     });
   };
// [SECTION] Functionalities [UPDATE]
// [SECTION] Functionalities [DELETE]













