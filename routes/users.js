// [SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/users');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Routes - [POST]

	route.post('/register', (req, res) => {
		let userDetails = req.body;
	
		controller.registerUser(userDetails).then(outcome => {
			res.send(outcome);
		});
	});



// [SECTION] Routes - [GET]
	route.get('/all', (req, res) => {
   
	    controller.getAllUsers().then(result => {
	    	res.send(result);
	    });
   });

// [SECTION] Routes - [PUT]
// [SECTION] Routes - [DELETE]

// [SECTION] Routes Expose Route System
	module.exports = route;









